// Random integer in [min, max]
Math.randInt = function(min, max){
    return min + Math.floor(Math.random() * (max - min + 1));
};

// Pick random element from array
Array.prototype.randomChoice = function(){
    return this[Math.randInt(0, this.length-1)];
};

var HexMap = function(divId, width, height){
    this.el = $(divId);
    this.el.css({
        'position': 'relative'
    });
    this.width = width;
    this.height = height;
    this.tileW = 65;
    this.tileH = 89;
    this.tileWHalf = 32;
    this.tileHTop = 48;
    this.tiles = [];
    // fill map (all spaces must have tiles, but they default to "unset")
    for(var y = 0; y < height; y++){
        this.tiles[y] = [];
        for(var x = 0; x < width; x++){
            this.tiles[y][x] = new HexMapTile(this, x, y, 'unset');
        }
    }
    // expand map div to fit everything
    this.el.width(width * this.tileW + this.tileWHalf);
    this.el.height((height-1) * this.tileHTop + this.tileH);
};
HexMap.prototype.getTile = function(x, y){
    if(x < 0 || x >= this.width || y < 0 || y >= this.height)
        throw "Tile index out of bounds";
    return this.tiles[y][x];
};
HexMap.prototype.getAllTiles = function(){
    var results = [];
    $.each(this.tiles, function(i, row){
        results = results.concat(row);
    });
    return results;
};
HexMap.prototype.getRandomTile = function(){
    return this.getTile(Math.randInt(0, this.width-1), Math.randInt(0, this.height-1));
};
HexMap.prototype.getEdgeTiles = function(){
    // top and bottom rows
    var results = this.tiles[0];
    results = results.concat(this.tiles[this.height-1]);
    // left and right columns (excluding tiles already added)
    for(var y = 1; y < this.height-1; y++){
        results.push(this.tiles[y][0]);
        results.push(this.tiles[y][this.width-1]);
    }
    return results;
};
HexMap.prototype.getTilesByType = function(type){
    var results = [];
    $.each(this.getAllTiles(), function(i, tile){
        if(tile.type == type)
            results.push(tile);
    });
    return results;
};

var HexMapTile = function(map, x, y, type){
    this.map = map;
    this.x = x;
    this.y = y;
    this.el = $('<div class="tile"></div>').appendTo(map.el);
    this.el.css({
        'position': 'absolute',
        'left': this.x * map.tileW + (this.y % 2 == 1 ? map.tileWHalf : 0),
        'top': this.y * map.tileHTop,
        'width': map.tileW,
        'height': map.tileH,
        'z-index': this.y
    });
    this.setType(type);

    var tile = this;
    this.el.click(function(e){
        map._tileClicked(tile, e);
    });
};
HexMapTile.prototype.neighbors = function(){
    var offsets = [
        [1, 0], [-1, 0],
        [0, -1], [0, 1]
    ];
    if(this.y % 2 == 0){
        offsets.push([-1, -1]);
        offsets.push([-1, 1]);
    } else{
        offsets.push([1, -1]);
        offsets.push([1, 1]);
    }

    var results = [];
    var map = this.map;
    var x = this.x, y = this.y;
    $.each(offsets, function(i, ofs){
        var t;
        try{
            t = map.getTile(x+ofs[0], y+ofs[1]);
        } catch(e){ return; }
        if(t.type != 'none')
            results.push(t);
    });

    return results;
};
HexMapTile.prototype.setType = function(type){
    this.type = type;
    this.el.empty();
    var tileImage;
    switch(type){
        case 'unset':
            tileImage = 'Magic_tile';
            break;
        case 'grass':
        case 'meadow':
            tileImage = 'Grass';
            this._decorateGrassTile();
            break;
        case 'water':
            tileImage = 'Water_full';
            this.el.css('opacity', 0.9);
            if(Math.random() > 0.3)
                for(var i = 0; i < Math.randInt(1, 2); i++)
                    this._addWave();
            break;
        case 'dirt':
            tileImage = 'Dirt_full';
            if(Math.random() > 0.3)
                this._addHill('Dirt');
            break;
        case 'grassForest':
        case 'forest':
            tileImage = 'Grass';
            this._addPineTree('Green');
            break;
        case 'snowForest':
            tileImage = 'Snow';
            this._addPineTree('Blue');
            break;
        case 'sand':
            tileImage = 'Sand';
            if(Math.random() > 0.4)
                this._addHill('Sand');
            break;
        case 'desert':
            tileImage = 'Sand';
            if(Math.random() > 0.5)
                this._addHill('Sand');
            else if(Math.random() > 0.7)
                this._addCactus();
            break;
        default:
            tileImage = type.substr(0,1).toUpperCase()+type.substr(1);
    }
    this.el.css('background-image', 'url(tiles/tile'+tileImage+'.png)');
    return this;
};
HexMapTile.prototype._addWave = function(waveType){
    waveType = waveType || 'Water';
    $('<div></div>').css({
        'position': 'absolute',
        'top': Math.randInt(16, 30),
        'left': Math.randInt(0, 30),
        'background': 'url(tiles/wave'+waveType+'.png)',
        'width': 33,
        'height': 10
    }).appendTo(this.el);
};
HexMapTile.prototype._decorateGrassTile = function(){
    if(Math.random() > 0.3){
        switch(Math.randInt(1, 4)){
            case 1:
            case 2:
                $("<div></div>").css({
                    'position': 'absolute',
                    'top': Math.randInt(16, 28),
                    'left': Math.randInt(3, 46),
                    'background-image': 'url(tiles/bushGrass.png)',
                    'width': 15,
                    'height': 20
                }).appendTo(this.el);
                break;
            case 3:
                for(var i = 0; i < Math.randInt(1, 2); i++){
                    $("<div></div>").css({
                        'position': 'absolute',
                        'top': Math.randInt(16, 30),
                        'left': Math.randInt(3, 50),
                        'background-image': 'url(tiles/flowerWhite.png)',
                        'width': 12,
                        'height': 11
                    }).appendTo(this.el);
                }
                break;
            case 4:
                for(var i = 0; i < Math.randInt(1, 2); i++){
                    $("<div></div>").css({
                        'position': 'absolute',
                        'top': Math.randInt(16, 30),
                        'left': Math.randInt(3, 50),
                        'background-image': 'url(tiles/flowerYellow.png)',
                        'width': 12,
                        'height': 11
                    }).appendTo(this.el);
                }
                break;
        }
    }
};
HexMapTile.prototype._addHill = function(hillType){
    $('<div></div>').css({
        'position': 'absolute',
        'top': Math.randInt(16, 30),
        'left': Math.randInt(0, 32),
        'background-image': 'url(tiles/hill'+hillType+'.png)',
        'width': 33,
        'height': 12
    }).appendTo(this.el);
};
HexMapTile.prototype._addPineTree = function(color){
    color = color || 'Green';
    switch(Math.randInt(1, 2)){
    case 1:
        $('<div></div>').css({
            'position': 'absolute',
            'top': Math.randInt(-65, -48),
            'left': Math.randInt(0, 35),
            'background-image': 'url(tiles/pine'+color+'_mid.png)',
            'width': 30,
            'height': 101,
            'z-index': 2
        }).appendTo(this.el);
        break;
    case 2:
        $('<div></div>').css({
            'position': 'absolute',
            'top': Math.randInt(-43, -26),
            'left': Math.randInt(0, 35),
            'background-image': 'url(tiles/pine'+color+'_low.png)',
            'width': 30,
            'height': 79,
            'z-index': 2
        }).appendTo(this.el);
        break;
    }
};
HexMapTile.prototype._addCactus = function(){
    $('<div></div>').css({
        'position': 'absolute',
        'top': Math.randInt(-20, -13),
        'left': Math.randInt(0, 33),
        'background-image': 'url(tiles/treeCactus_'+Math.randInt(1,3)+'.png)',
        'width': 32,
        'height': 66,
        'z-index': 2
    }).appendTo(this.el);
};
